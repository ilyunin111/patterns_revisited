package com.nyarian.self_education.patterns.behavioral.interpreter.factory;

import com.nyarian.self_education.patterns.behavioral.interpreter.RegularExpression;

import java.util.List;

interface RegularExpressionFinalizer {

    void finalize(List<RegularExpression> expressions);

}
