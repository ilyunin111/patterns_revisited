package com.nyarian.self_education.patterns.behavioral.interpreter.factory;

import com.nyarian.self_education.patterns.behavioral.interpreter.RegularExpression;
import com.nyarian.self_education.patterns.behavioral.interpreter.RepetitionExpression;

class RepetitionTransformer implements RegularExpressionTransformer {

    @Override
    public RegularExpression transform(RegularExpression subject) {
        return RepetitionExpression.of(subject);
    }
}
