package com.nyarian.self_education.patterns.behavioral.interpreter;

public class EmptyExpressionException extends IllegalArgumentException {

    public EmptyExpressionException() {
    }

    public EmptyExpressionException(String s) {
        super(s);
    }

    public EmptyExpressionException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public EmptyExpressionException(Throwable throwable) {
        super(throwable);
    }
}
