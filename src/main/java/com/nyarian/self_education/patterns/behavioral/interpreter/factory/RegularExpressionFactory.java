package com.nyarian.self_education.patterns.behavioral.interpreter.factory;

import com.nyarian.self_education.patterns.behavioral.interpreter.*;

//TODO [BUG] nested expressions parsing failure
public class RegularExpressionFactory {

    private final RegularExpressionParser parser;

    private RegularExpressionFactory(RegularExpressionParser parser) {
        this.parser = parser;
    }

    public static RegularExpressionFactory create() {
        return new RegularExpressionFactory(RegularExpressionParser.create());
    }

    public RegularExpression of(String stringified) {
        assertArgumentViability(stringified);
        final RegularExpressionTree tree = parse(stringified);
        return extractInterpreter(stringified, tree);
    }

    public RegularExpression extractInterpreter(String stringified, RegularExpressionTree tree) {
        if (tree.isEmpty()) {
            throw new EmptyExpressionException("Expressions sequence is empty. Given string: \"" + stringified + "\"");
        } else {
            return tree.summarize();
        }
    }

    public void assertArgumentViability(String stringified) {
        if (stringified == null || stringified.isEmpty()) {
            throw new EmptyExpressionException(stringified == null ? "Received null" : "Received empty string");
        }
    }

    private RegularExpressionTree parse(String stringified) {
        try {
            return parser.parse(stringified);
        } catch (RegularExpressionFormatException e) {
            throw new RegularExpressionFormatException("Invalid expression format. Given argument: " + stringified +
                    ".", e);
        }
    }

}
