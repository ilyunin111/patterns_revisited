package com.nyarian.self_education.patterns.behavioral.interpreter;

public class NoMatchException extends IllegalArgumentException {

    public NoMatchException() {
    }

    public NoMatchException(String s) {
        super(s);
    }

    public NoMatchException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public NoMatchException(Throwable throwable) {
        super(throwable);
    }
}
