package com.nyarian.self_education.patterns.behavioral.interpreter;

import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public class MatchSequence {

    private final String source;
    private final int startIndex;

    private MatchSequence(String source, int startIndex) {
        this.source = source;
        this.startIndex = startIndex;
    }

    public static MatchSequence of(String source) {
        return MatchSequence.of(source, 0);
    }

    public static MatchSequence of(String source, int startIndex) {
        assertInvariants(source, startIndex);
        return new MatchSequence(source, startIndex);
    }

    private static void assertInvariants(String source, int startIndex) {
        assertSourceIsNotNullOrEmpty(source);
        assertStartIndexIsNotNegative(source, startIndex);
        assertStartingIndexIsNotGreaterThanSourceLength(source, startIndex);
    }

    private static void assertSourceIsNotNullOrEmpty(String source) {
        if (source == null || source.isEmpty()) {
            throw new IllegalArgumentException(String.format("Given source string is %1$s, which is forbidden",
                    source));
        }
    }

    private static void assertStartIndexIsNotNegative(String source, int startIndex) {
        if (startIndex < 0) {
            throw new IllegalArgumentException(
                    String.format(
                            "Starting index can't be less than 0 but got %1$d (source: \"%2$s\")", startIndex, source
                    ));
        }
    }

    private static void assertStartingIndexIsNotGreaterThanSourceLength(String source, int startIndex) {
        if (startIndex > source.length()) {
            throw new IllegalArgumentException(String.format(
                    "Starting index can't be greater then source string's length: `startIndex`: %d, `source`: %s",
                    startIndex, source));
        }
    }

    public String fullSource() {
        return source;
    }

    public int startIndex() {
        return startIndex;
    }

    public String partToMatch() {
        return source.substring(startIndex);
    }

    public boolean startsWith(String part) {
        return source.startsWith(part, startIndex);
    }

    public Match match(int to) {
        return Match.of(source, startIndex, to);
    }

    public MatchSequence shiftBy(int count) {
        if (count == 0) return this;
        return MatchSequence.of(this.source, this.startIndex + count);
    }

    public int length() {
        return source.length();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MatchSequence that = (MatchSequence) o;

        if (startIndex != that.startIndex) return false;
        return Objects.equals(source, that.source);
    }

    @Override
    public int hashCode() {
        int result = source != null ? source.hashCode() : 0;
        result = 31 * result + startIndex;
        return result;
    }

    @Override
    public String toString() {
        return "MatchSubject{" +
                "source='" + source + '\'' +
                ", startIndex=" + startIndex +
                '}';
    }
}
