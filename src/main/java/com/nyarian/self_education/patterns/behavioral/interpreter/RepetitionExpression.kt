package com.nyarian.self_education.patterns.behavioral.interpreter

class RepetitionExpression private constructor(private val delegate: RegularExpression) : RegularExpression {
    override fun matches(subject: MatchSequence): Match =
        generateSequence(Pair(delegate.matches(subject), subject)) { (previousMatch, residue) ->
            if (previousMatch.to() > residue.length()) null
            else matchOrNull(residue)?.let { match -> Pair(match, residue.shiftBy(match.length())) }
        }
            .last()
            .first
            .startedFrom(subject.startIndex())

    private fun matchOrNull(sequence: MatchSequence): Match? = try {
        delegate.matches(sequence)
    } catch (e: NoMatchException) {
        null
    }

    companion object {
        @JvmStatic
        fun of(delegate: RegularExpression) = RepetitionExpression(delegate)
    }

}
