package com.nyarian.self_education.patterns.behavioral.interpreter;

public class RegularExpressionFormatException extends IllegalArgumentException {

    public RegularExpressionFormatException() {
    }

    public RegularExpressionFormatException(String s) {
        super(s);
    }

    public RegularExpressionFormatException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public RegularExpressionFormatException(Throwable throwable) {
        super(throwable);
    }
}
