package com.nyarian.self_education.patterns.behavioral.interpreter.factory;

import com.nyarian.self_education.patterns.behavioral.interpreter.RegularExpression;

interface RegularExpressionTransformer {

    RegularExpression transform(RegularExpression subject);

}
