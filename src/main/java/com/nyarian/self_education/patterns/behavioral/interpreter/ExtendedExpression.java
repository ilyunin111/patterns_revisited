package com.nyarian.self_education.patterns.behavioral.interpreter;

import java.util.Optional;

public class ExtendedExpression implements RegularExpression {

    private final RegularExpression delegate;

    private ExtendedExpression(RegularExpression delegate) {
        this.delegate = delegate;
    }

    public static ExtendedExpression of(RegularExpression delegate) {
        return new ExtendedExpression(delegate);
    }

    public Match matches(String source) {
        return delegate.matches(MatchSequence.of(source, 0));
    }

    public Match matches(String source, int from) {
        return delegate.matches(MatchSequence.of(source, from));
    }

    public Optional<Match> optionalMatch(String source) {
        return optionalMatch(source, 0);
    }

    public Optional<Match> optionalMatch(String source, int from) {
        return optionalMatch(MatchSequence.of(source, from));
    }

    public Optional<Match> optionalMatch(MatchSequence subject) {
        try {
            return Optional.of(matches(subject));
        } catch (NoMatchException unused) {
            return Optional.empty();
        }
    }

    public boolean hasMatch(String source) {
        return hasMatch(source, 0);
    }

    public boolean hasMatch(String source, int from) {
        try {
            matches(MatchSequence.of(source, from));
            return true;
        } catch (NoMatchException unused) {
            return false;
        }
    }

    @Override
    public Match matches(MatchSequence subject) {
        return delegate.matches(subject);
    }
}
