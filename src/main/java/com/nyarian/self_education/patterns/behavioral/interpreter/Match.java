package com.nyarian.self_education.patterns.behavioral.interpreter;

import java.util.Objects;

public class Match {

    private final String source;
    // Inclusive
    private final int from;
    // Exclusive
    private final int to;

    private Match(String source, int from, int to) {
        this.source = source;
        this.from = from;
        this.to = to;
    }

    public static Match of(String source, int from, int to) {
        assertInvariants(source, from, to);
        return new Match(source, from, to);
    }

    private static void assertInvariants(String source, int from, int to) {
        if (to > source.length()) {
            throw new IllegalArgumentException(
                    String.format("`to` argument is bigger than `source` length: `to`: %1$d, `source`: %2$s",
                            to, source)
            );
        }
        if (from < 0) {
            throw new IllegalArgumentException(String.format("`from` can't be less than 0, but got %1$d", from));
        }
        if (from >= to) {
            throw new IllegalArgumentException(
                    String.format("`from` can't be more than or equal to `to`: `from`: %1$d, `to`: %2$d",
                            from, to)
            );
        }
    }

    public String matchedPart() {
        return source.substring(from, to + 1);
    }

    public int length() {
        return to - from;
    }

    public Match startedFrom(int startIndex) {
        return Match.of(this.source, startIndex, this.to);
    }

    public int from() {
        return this.from;
    }

    public int to() {
        return this.to;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Match match = (Match) o;

        if (from != match.from) return false;
        if (to != match.to) return false;
        return Objects.equals(source, match.source);
    }

    @Override
    public int hashCode() {
        int result = source != null ? source.hashCode() : 0;
        result = 31 * result + from;
        result = 31 * result + to;
        return result;
    }

    @Override
    public String toString() {
        return "Match{" +
                "source='" + source + '\'' +
                ", from=" + from +
                ", to=" + to +
                '}';
    }
}
