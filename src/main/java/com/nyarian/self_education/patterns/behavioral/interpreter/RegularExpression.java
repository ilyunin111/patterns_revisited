package com.nyarian.self_education.patterns.behavioral.interpreter;

import com.nyarian.self_education.patterns.behavioral.interpreter.factory.RegularExpressionFactory;

// TODO create finalized expression which asserts that the underlying match embraced the whole source string
// TODO create "any symbol" and similar expressions
// TODO support escaping
// TODO support symbol ranges
// TODO support repetitions and ranges count
// TODO support groups
// TODO support casing ignore
public interface RegularExpression {

    Match matches(MatchSequence subject);

    static RegularExpression of(String stringified) {
        return RegularExpressionFactory.create().of(stringified);
    }

}
