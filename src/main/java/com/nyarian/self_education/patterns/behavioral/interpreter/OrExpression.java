package com.nyarian.self_education.patterns.behavioral.interpreter;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class OrExpression implements RegularExpression {

    private final RegularExpression[] expressions;

    private OrExpression(RegularExpression[] expressions) {
        this.expressions = expressions;
    }

    public static OrExpression of(RegularExpression... expressions) {
        return new OrExpression(expressions);
    }

    public static OrExpression of(List<RegularExpression> expressions) {
        return OrExpression.of(expressions.toArray(new RegularExpression[0]));
    }

    @Override
    public Match matches(MatchSequence subject) {
        return Stream.of(expressions)
                .map(ExtendedExpression::of)
                .map(expression -> expression.optionalMatch(subject))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .findFirst()
                .orElseThrow(NoMatchException::new);
    }
}
