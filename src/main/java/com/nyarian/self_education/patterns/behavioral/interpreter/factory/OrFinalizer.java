package com.nyarian.self_education.patterns.behavioral.interpreter.factory;

import com.nyarian.self_education.patterns.behavioral.interpreter.OrExpression;
import com.nyarian.self_education.patterns.behavioral.interpreter.RegularExpression;

import java.util.Arrays;
import java.util.List;

class OrFinalizer implements RegularExpressionFinalizer {
    @Override
    public void finalize(List<RegularExpression> expressions) {
        if (expressions.size() < 2) {
            throw new IllegalArgumentException("Expressions amount is less than 2: " + expressions);
        }
        final int preLastIndex = expressions.size() - 2;
        final int lastIndex = expressions.size() - 1;
        final OrExpression or = OrExpression.of(
                Arrays.asList(expressions.get(preLastIndex), expressions.get(lastIndex))
        );
        expressions.set(preLastIndex, or);
        expressions.remove(lastIndex);
    }
}
