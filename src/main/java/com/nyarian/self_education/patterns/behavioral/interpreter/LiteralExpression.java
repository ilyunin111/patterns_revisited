package com.nyarian.self_education.patterns.behavioral.interpreter;

public class LiteralExpression implements RegularExpression {

    private final String literal;

    private LiteralExpression(String literal) {
        this.literal = literal;
    }

    public static LiteralExpression of(String literal) {
        assertLiteralInvariants(literal);
        return new LiteralExpression(literal);
    }

    private static void assertLiteralInvariants(String literal) {
        if (literal == null || literal.isEmpty()) {
            throw new EmptyExpressionException(
                    String.format("Given literal is null or empty (%s), which is forbidden.", literal));
        }
    }

    @Override
    public Match matches(MatchSequence subject) {
        assertThatSourcePartLengthIsGreaterThanLiteral(subject);
        if (subject.startsWith(literal)) {
            return subject.match(subject.startIndex() + literal.length());
        } else {
            throw new NoMatchException(String.format("Given string does not match the given literal at the given " +
                            "position: `literal`: %1$s, `from`: %2$d, `source`: %3$s",
                    literal, subject.startIndex(), subject.fullSource()));
        }
    }

    private void assertThatSourcePartLengthIsGreaterThanLiteral(MatchSequence subject) {
        if (subject.fullSource().length() - subject.startIndex() < literal.length()) {
            throw new NoMatchException(String.format(
                    "Source part is less than literal to match against. Literal: \"%1$s\", source part: \"%2$s\"",
                    literal, subject.partToMatch()));
        }
    }

}
