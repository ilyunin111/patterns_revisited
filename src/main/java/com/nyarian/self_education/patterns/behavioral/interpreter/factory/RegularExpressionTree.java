package com.nyarian.self_education.patterns.behavioral.interpreter.factory;

import com.nyarian.self_education.patterns.behavioral.interpreter.RegularExpression;
import com.nyarian.self_education.patterns.behavioral.interpreter.SequenceExpression;

import java.util.ArrayList;
import java.util.List;

class RegularExpressionTree {

    private final List<RegularExpression> expressions;
    private final List<RegularExpressionFinalizer> finalizers = new ArrayList<>();

    private RegularExpressionTree(List<RegularExpression> expressions) {
        this.expressions = expressions;
    }

    public static RegularExpressionTree empty() {
        return new RegularExpressionTree(new ArrayList<>());
    }

    public void add(RegularExpressionFinalizer finalizer) {
        finalizers.add(finalizer);
    }

    public void add(RegularExpression expression) {
        expressions.add(expression);
        finalizers.forEach(finalizer -> finalizer.finalize(this.expressions));
        finalizers.clear();
    }

    public boolean isEmpty() {
        return expressions.isEmpty();
    }

    public void consume(RegularExpressionTree nestedTree) {
        this.add(nestedTree.summarize());
    }

    public void transformLast(RegularExpressionTransformer transformer) {
        final int lastElementIndex = this.expressions.size() - 1;
        final RegularExpression lastElement = this.expressions.get(lastElementIndex);
        this.expressions.set(lastElementIndex, transformer.transform(lastElement));
    }

    public RegularExpression summarize() {
        if (isEmpty()) throw new IllegalStateException("No expressions given");
        if (!finalizers.isEmpty()) throw new IllegalStateException("Not all finalizers were used: " + finalizers);
        if (expressions.size() == 1) {
            return expressions.get(0);
        } else {
            return SequenceExpression.of(expressions);
        }
    }
}
