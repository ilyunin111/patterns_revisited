package com.nyarian.self_education.patterns.behavioral.interpreter.factory;

import com.nyarian.self_education.patterns.behavioral.interpreter.LiteralExpression;
import com.nyarian.self_education.patterns.behavioral.interpreter.RegularExpressionFormatException;

import java.util.HashSet;
import java.util.Set;

class RegularExpressionParser {

    private static final char START_OF_EXPRESSION = '(';
    private static final char END_OF_EXPRESSION = ')';
    private static final char REPETITION = '*';
    private static final char OR = '|';
    private static final Set<Character> OPERATORS = new HashSet<>();

    static {
        OPERATORS.add(START_OF_EXPRESSION);
        OPERATORS.add(END_OF_EXPRESSION);
        OPERATORS.add(REPETITION);
        OPERATORS.add(OR);
    }

    private RegularExpressionParser() {

    }

    static RegularExpressionParser create() {
        return new RegularExpressionParser();
    }

    RegularExpressionTree parse(String stringified) {
        final RegularExpressionTree tree = RegularExpressionTree.empty();
        parseTokensInto(stringified, tree);
        return tree;
    }

    // TODO move to a parser class which delegates to each expression parsing strategy separately
    private void parseTokensInto(String source, RegularExpressionTree tree) {
        final StringBuilder literalBuilder = new StringBuilder();
        int parseIndex = 0;
        do {
            final char character = source.charAt(parseIndex);
            switch (character) {
                case START_OF_EXPRESSION:
                    parseIndex = parseNestedExpression(tree, source, parseIndex);
                    break;
                case OR:
                    tree.add(new OrFinalizer());
                    break;
                case END_OF_EXPRESSION:
                    // No-op by design: simply shift the currently parsed index forward
                    break;
                case REPETITION:
                    parseRepetition(tree);
                    break;
                default:
                    parseLiteral(source, tree, literalBuilder, parseIndex);
            }
            parseIndex++;
        } while (parseIndex < source.length());
    }

    private void parseLiteral(String source, RegularExpressionTree tree, StringBuilder literalBuilder, int parseIndex) {
        literalBuilder.append(source.charAt(parseIndex));
        final int nextIndex = parseIndex + 1;
        if (nextIndex >= source.length() || OPERATORS.contains(source.charAt(nextIndex))) {
            tree.add(LiteralExpression.of(literalBuilder.toString()));
            literalBuilder.setLength(0);
        }
    }

    private int parseNestedExpression(RegularExpressionTree tree, String source, int parseIndex) {
        final int endOfExpressionIndex = findEndOfExpressionIndex(source, parseIndex);
        final RegularExpressionTree nestedTree = RegularExpressionTree.empty();
        parseTokensInto(source.substring(parseIndex + 1, endOfExpressionIndex), nestedTree);
        tree.consume(nestedTree);
        return endOfExpressionIndex;
    }

    private int findEndOfExpressionIndex(String source, int parseIndex) {
        if (source.length() >= parseIndex + 1) {
            int nestingLevel = 1;
            for (int i = parseIndex + 1; i < source.length(); ++i) {
                final char character = source.charAt(i);
                if (character == START_OF_EXPRESSION) {
                    nestingLevel++;
                } else if (character == END_OF_EXPRESSION && --nestingLevel == 0) {
                    return i;
                }
            }
        }
        throw new RegularExpressionFormatException("Could not find expression ending operator (\")\") " +
                "for the expression part \"" + source.substring(parseIndex) + "\"");
    }

    private void parseRepetition(RegularExpressionTree tree) {
        tree.transformLast(new RepetitionTransformer());
    }

}
