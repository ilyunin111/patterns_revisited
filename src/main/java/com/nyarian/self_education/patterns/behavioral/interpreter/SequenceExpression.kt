package com.nyarian.self_education.patterns.behavioral.interpreter

import java.util.*

class SequenceExpression private constructor(private val expressions: List<RegularExpression>) :
    RegularExpression {
    override fun matches(subject: MatchSequence): Match = expressions.asSequence()
        .drop(1)
        .fold(expressions.first().matches(subject)) { previousMatch, expression ->
            expression.matches(subject.shiftBy(previousMatch.to()))
        }
        .startedFrom(subject.startIndex())

    companion object {
        @JvmStatic
        fun of(vararg expressions: RegularExpression): SequenceExpression = of(listOf(*expressions))

        @JvmStatic
        fun of(expressions: List<RegularExpression>): SequenceExpression {
            require(expressions.size > 1) {
                "Empty or single-element sequences are forbidden, but " +
                        "received ${expressions.joinToString()}"
            }
            return SequenceExpression(ArrayList(expressions))
        }
    }

}
