package com.nyarian.self_education.patterns.behavioral.interpreter;

public class MatchExpression implements RegularExpression {

    public static final MatchExpression MATCHES = MatchExpression.randomMatch();

    private final Match answer;

    private MatchExpression(Match answer) {
        this.answer = answer;
    }

    public static MatchExpression of(Match match) {
        return new MatchExpression(match);
    }

    public static MatchExpression randomMatch() {
        return new MatchExpression(
                Match.of(
                        "randomMatch",
                        0,
                        "randomMatch".length() - 1
                )
        );
    }

    @Override
    public Match matches(MatchSequence subject) {
        return this.answer;
    }
}
