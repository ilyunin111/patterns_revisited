package com.nyarian.self_education.patterns.behavioral.interpreter;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RepetitionExpressionTest {

    @Test
    @DisplayName("assert that a sequence was matched once by an underlying literal expression if it corresponds " +
            "1:1 to the source")
    public void test1() {
        String source = "source";
        assertEquals(
                Match.of(source, 0, 6),
                RepetitionExpression.of(LiteralExpression.of(source)).matches(MatchSequence.of(source))
        );
    }

    @Test
    @DisplayName("assert that a sequence was matched twice by an underlying literal expression if the source is a " +
            "merge of two literals to match against")
    public void test2() {
        String source = "source";
        assertEquals(
                Match.of(source + source, 0, 12),
                RepetitionExpression.of(LiteralExpression.of(source)).matches(MatchSequence.of(source + source))
        );
    }

    @Test
    @DisplayName("assert that a sequence was matched twice if the underlying literal is \"abc\" and the source is" +
            " \"abcabccba\"")
    public void test3() {
        String source = "abcabccba";
        assertEquals(
                Match.of(source, 0, 6),
                RepetitionExpression.of(LiteralExpression.of("abc")).matches(MatchSequence.of(source))
        );
    }

    @Test
    @DisplayName("assert that NoMatchException was thrown if the underlying expression does not match the source at " +
            "all")
    public void test4() {
        assertThrows(NoMatchException.class, () ->
                RepetitionExpression.of(LiteralExpression.of("abc")).matches(MatchSequence.of("cba")));
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    @DisplayName("construction, assert that IllegalArgumentException was thrown if null expression was given")
    public void test5() {
        assertThrows(IllegalArgumentException.class, () -> RepetitionExpression.of(null));
    }

    @Test
    @DisplayName("assert match from the middle of the string")
    public void test6() {
        final String givenSource = "aabbbbb";
        assertEquals(Match.of(givenSource, 2, 7),
                RepetitionExpression.of(LiteralExpression.of("b")).matches(MatchSequence.of(givenSource, 2)));
    }

}
