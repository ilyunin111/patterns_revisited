package com.nyarian.self_education.patterns.behavioral.interpreter;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MatchSequenceTest {

    @Test
    @DisplayName("construction: assert that IllegalArgumentException was thrown if starting index is less than 0")
    public void test1() {
        assertThrows(IllegalArgumentException.class, () -> MatchSequence.of("source", -1));
    }

    @Test
    @DisplayName("construction: assert that IllegalArgumentException was thrown if null source reference was given")
    public void test2() {
        assertThrows(IllegalArgumentException.class, () -> MatchSequence.of(null, 0));
    }

    @Test
    @DisplayName("construction: assert that IllegalArgumentException was thrown if empty source was given")
    public void test3() {
        assertThrows(IllegalArgumentException.class, () -> MatchSequence.of("", 0));
    }

    @Test
    @DisplayName("construction: assert that IllegalArgumentException was thrown if start index is greater than source" +
            " string's max index")
    public void test4() {
        assertThrows(IllegalArgumentException.class, () -> MatchSequence.of("a", 2));
    }

    @Test
    @DisplayName("construction: assert variables' values on a valid subject construction")
    public void test5() {
        MatchSequence subject = MatchSequence.of("source", 3);
        assertEquals("source", subject.fullSource());
        assertEquals(3, subject.startIndex());
        assertEquals("rce", subject.partToMatch());
    }

    @Test
    @DisplayName("construction: assert variables' values on an emptied sequence")
    public void test6() {
        MatchSequence subject = MatchSequence.of("source", 6);
        assertEquals("source", subject.fullSource());
        assertEquals(6, subject.startIndex());
        assertEquals("", subject.partToMatch());
    }

    @Test
    @DisplayName("startsWith: assert true if \"source\" starts with \"rce\" from index 3")
    public void test7() {
        MatchSequence subject = MatchSequence.of("source", 3);
        assertTrue(subject.startsWith("rce"));
    }

    @Test
    @DisplayName("startsWith: assert false if \"source\" starts with \"rce\" from index 2")
    public void test8() {
        MatchSequence subject = MatchSequence.of("source", 2);
        assertFalse(subject.startsWith("rce"));
    }

    @Test
    @DisplayName("match: assert that IllegalArgumentException was thrown if the `to` argument is greater than the " +
            "source's length")
    public void test10() {
        String source = "source";
        MatchSequence subject = MatchSequence.of(source, 2);
        assertThrows(IllegalArgumentException.class, () -> subject.match(source.length() + 1));
    }

    @Test
    @DisplayName("match: assert that IllegalArgumentException was thrown if the `to` argument is lesser than the " +
            "start index")
    public void test11() {
        MatchSequence subject = MatchSequence.of("source", 2);
        assertThrows(IllegalArgumentException.class, () -> subject.match(1));
    }

    @Test
    @DisplayName("match: assert that valid Match was returned if `to` is equal to the source's length")
    public void test12() {
        String source = "source";
        MatchSequence subject = MatchSequence.of(source, 2);
        assertEquals(Match.of(source, 2, source.length()), subject.match(source.length()));
    }

    @Test
    @DisplayName("match: assert that valid Match was returned if `to` is less than source's length and greater than" +
            " starting index")
    public void test13() {
        String source = "source";
        MatchSequence subject = MatchSequence.of(source, 2);
        assertEquals(Match.of(source, 2, 4), subject.match(4));
    }

    @Test
    @DisplayName("match: assert that IllegalArgumentException was thrown if the `to` argument equal to starting index")
    public void test14() {
        MatchSequence subject = MatchSequence.of("source", 2);
        assertThrows(IllegalArgumentException.class, () -> subject.match(2));
    }

    @Test
    @DisplayName("shiftBy: assert that shift from 0 to the all source string's length will return a valid Match object")
    public void test15() {
        String source = "source";
        assertEquals(MatchSequence.of(source, source.length()), MatchSequence.of(source).shiftBy(source.length()));
    }

    @Test
    @DisplayName("shiftBy: assert that IllegalArgumentException was thrown if shifted from 0 to the all source " +
            "string's length + 1")
    public void test16() {
        String source = "source";
        assertThrows(IllegalArgumentException.class, () -> MatchSequence.of(source).shiftBy(source.length() + 1));
    }

    @Test
    @DisplayName("shiftBy: assert that the same object was returned if shifted by 0")
    public void test17() {
        String source = "source";
        assertEquals(MatchSequence.of(source), MatchSequence.of(source).shiftBy(0));
    }

    @Test
    @DisplayName("shiftBy: assert that MatchSequence starting from 0 was returned if a sequence starting from 3rd " +
            "index was shifted backwards by 3")
    public void test18() {
        String source = "source";
        assertEquals(MatchSequence.of(source), MatchSequence.of(source, 3).shiftBy(-3));
    }

    @Test
    @DisplayName("shiftBy: assert that IllegalArgumentsException was thrown if the sequence was shifted up to the " +
            "negative starting index")
    public void test19() {
        String source = "source";
        assertThrows(IllegalArgumentException.class, () -> MatchSequence.of(source).shiftBy(-1));
    }

}
