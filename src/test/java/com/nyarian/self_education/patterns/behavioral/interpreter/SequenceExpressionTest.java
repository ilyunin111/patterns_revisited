package com.nyarian.self_education.patterns.behavioral.interpreter;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static com.nyarian.self_education.patterns.behavioral.interpreter.NoMatchExpression.NOT_MATCHES;
import static org.junit.jupiter.api.Assertions.*;

class SequenceExpressionTest {

    @Test
    @DisplayName("matches: assert that summary match object was returned if 2 literal matchers sequentially " +
            "correspond the match sequence")
    public void test1() {
        String source = "source";
        assertEquals(
                Match.of(source, 0, 6),
                SequenceExpression.of(
                        MatchExpression.of(Match.of(source, 0, 3)),
                        MatchExpression.of(Match.of(source, 3, 6))
                ).matches(MatchSequence.of(source))
        );
    }

    @Test
    @DisplayName("matches: assert that summary match object was returned if 3 literal matchers sequentially " +
            "correspond the match sequence")
    public void test2() {
        String source = "source";
        assertEquals(
                Match.of(source, 0, 6),
                SequenceExpression.of(
                        MatchExpression.of(Match.of(source, 0, 2)),
                        MatchExpression.of(Match.of(source, 2, 4)),
                        MatchExpression.of(Match.of(source, 4, 6))
                ).matches(MatchSequence.of(source))
        );
    }

    @Test
    @DisplayName("matches: assert that summary match object was returned if 3 literal matchers sequentially " +
            "correspond the start of the match sequence")
    public void test6() {
        String source = "source with ending";
        assertEquals(
                Match.of(source, 0, 6),
                SequenceExpression.of(
                        MatchExpression.of(Match.of(source, 0, 2)),
                        MatchExpression.of(Match.of(source, 2, 4)),
                        MatchExpression.of(Match.of(source, 4, 6))
                ).matches(MatchSequence.of(source))
        );
    }

    @Test
    @DisplayName("matches: assert that NoMatchException was thrown if the last matcher does not match the given " +
            "sequence part")
    public void test7() {
        String source = "source";
        assertThrows(NoMatchException.class, () -> SequenceExpression.of(
                MatchExpression.of(Match.of(source, 0, 2)),
                MatchExpression.of(Match.of(source, 2, 4)),
                NOT_MATCHES
                ).matches(MatchSequence.of(source))
        );
    }

    @Test
    @DisplayName("matches: assert that NoMatchException was thrown if the first matcher does not match the given " +
            "sequence part")
    public void test8() {
        String source = "source";
        assertThrows(NoMatchException.class, () -> SequenceExpression.of(
                NOT_MATCHES,
                MatchExpression.of(Match.of(source, 2, 4)),
                MatchExpression.of(Match.of(source, 4, 6))
                ).matches(MatchSequence.of(source))
        );
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    @DisplayName("construction: assert that IllegalArgumentException was thrown if null list was provided")
    public void test3() {
        assertThrows(IllegalArgumentException.class, () -> SequenceExpression.of((List<RegularExpression>) null));
    }

    @Test
    @DisplayName("construction: assert that IllegalArgumentException was thrown if empty list was provided")
    public void test4() {
        assertThrows(IllegalArgumentException.class, () -> SequenceExpression.of(Collections.emptyList()));
    }

    @Test
    @DisplayName("construction: assert that IllegalArgumentException was thrown if single-element list was provided")
    public void test5() {
        assertThrows(IllegalArgumentException.class, () -> SequenceExpression.of(NOT_MATCHES));
    }

}
