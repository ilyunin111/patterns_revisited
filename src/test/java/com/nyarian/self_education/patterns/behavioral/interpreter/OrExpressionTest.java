package com.nyarian.self_education.patterns.behavioral.interpreter;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.nyarian.self_education.patterns.behavioral.interpreter.MatchExpression.MATCHES;
import static com.nyarian.self_education.patterns.behavioral.interpreter.NoMatchExpression.NOT_MATCHES;
import static org.junit.jupiter.api.Assertions.*;

public class OrExpressionTest {

    @Test
    @DisplayName("matches: assert valid match if the only literal expression matches the source string")
    public void test1() {
        String source = "literal";
        assertTrue(
                ExtendedExpression.of(
                        OrExpression.of(MATCHES)
                ).hasMatch(source)
        );
    }

    @Test
    @DisplayName("matches: assert valid match if the second of two literal expressions match the source string")
    public void test2() {
        String source = "literal";
        assertTrue(
                ExtendedExpression.of(
                        OrExpression.of(
                                NOT_MATCHES,
                                MATCHES
                        )
                ).hasMatch(source)
        );
    }

    @Test
    @DisplayName("matches: assert valid match if both of two literal expressions match the source string")
    public void test3() {
        String source = "literal";
        assertTrue(
                ExtendedExpression.of(
                        OrExpression.of(
                                MATCHES,
                                MATCHES
                        )
                ).hasMatch(source)
        );
    }

    @Test
    @DisplayName("matches: assert valid match if first OrExpression doesn't match and second OrExpression matches " +
            "the source string")
    public void test4() {
        String source = "literal";
        assertTrue(
                ExtendedExpression.of(
                        OrExpression.of(
                                OrExpression.of(
                                        NOT_MATCHES,
                                        NOT_MATCHES
                                ),
                                OrExpression.of(
                                        NOT_MATCHES,
                                        MATCHES
                                )
                        )
                ).hasMatch(source)
        );
    }

    @Test
    @DisplayName("matches: assert valid match if both of the OrExpression trees Match the source string")
    public void test5() {
        String source = "literal";
        assertTrue(
                ExtendedExpression.of(
                        OrExpression.of(
                                OrExpression.of(
                                        NOT_MATCHES,
                                        NOT_MATCHES
                                ),
                                OrExpression.of(
                                        NOT_MATCHES,
                                        MATCHES
                                )
                        )
                ).hasMatch(source)
        );
    }

    @Test
    @DisplayName("matches: assert that NoMatchException was thrown if no underlying expression matches the source " +
            "string")
    public void test6() {
        String source = "literal";
        assertThrows(NoMatchException.class, () ->
                ExtendedExpression.of(
                        OrExpression.of(
                                NOT_MATCHES,
                                NOT_MATCHES
                        )
                ).matches(source));
    }

}
