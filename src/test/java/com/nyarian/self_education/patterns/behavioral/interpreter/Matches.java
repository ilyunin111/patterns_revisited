package com.nyarian.self_education.patterns.behavioral.interpreter;

public final class Matches {

    private Matches() {

    }

    public static Match full(String source) {
        return Match.of(source, 0, source.length());
    }

}
