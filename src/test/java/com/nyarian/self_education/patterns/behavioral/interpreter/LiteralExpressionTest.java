package com.nyarian.self_education.patterns.behavioral.interpreter;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class LiteralExpressionTest {

    @Test
    @DisplayName("matches: assert valid match if the given literal corresponds to the source string")
    public void test1() {
        final String literal = "literal";
        assertEquals(Match.of(literal, 0, 7),
                ExtendedExpression.of(LiteralExpression.of(literal)).matches(literal, 0));
    }

    @Test
    @DisplayName("matches: assert valid match if the given literal corresponds to the source string shifted by 3 " +
            "symbols")
    public void test2() {
        final String literal = "literal";
        String prefix = "yes";
        String source = prefix + literal;
        assertEquals(Match.of(source, 3, 10),
                ExtendedExpression.of(LiteralExpression.of(literal)).matches(source, prefix.length()));
    }

    @Test
    @DisplayName("matches: assert that NoMatchException was thrown if the given literal does not correspond to the" +
            " source string at all")
    public void test3() {
        assertThrows(
                NoMatchException.class,
                () -> ExtendedExpression.of(LiteralExpression.of("literal")).matches("source", 0)
        );
    }

    @Test
    @DisplayName("matches: assert that IllegalArgumentException was thrown if `from` argument is greater than the" +
            " source string's length")
    public void test4() {
        assertThrows(
                IllegalArgumentException.class,
                () -> ExtendedExpression.of(LiteralExpression.of("literal")).matches("source", 6)
        );
    }

    @Test
    @DisplayName("construction: assert that EmptyExpressionException was thrown if empty literal was given")
    public void test5() {
        assertThrows(
                EmptyExpressionException.class,
                () -> LiteralExpression.of("")
        );
    }

    @Test
    @DisplayName("construction: assert that IllegalArgumentException was thrown if null pointing literal reference" +
            " was provided")
    public void test6() {
        assertThrows(
                IllegalArgumentException.class,
                () -> LiteralExpression.of(null)
        );
    }
    
    @Test
    @DisplayName("matches: assert that the expected match part is returned if match was invoked with a shift")
    public void test7() {
        final String source = "aaaaa";
        final LiteralExpression expression = LiteralExpression.of("a");
        for(int i = 0; i < source.length(); i++) {
            assertEquals(Match.of(source, i, i + 1), expression.matches(MatchSequence.of(source, i)));
        }
    }

}
