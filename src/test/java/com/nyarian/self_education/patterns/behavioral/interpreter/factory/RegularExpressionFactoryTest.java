package com.nyarian.self_education.patterns.behavioral.interpreter.factory;

import com.nyarian.self_education.patterns.behavioral.interpreter.ExtendedExpression;
import com.nyarian.self_education.patterns.behavioral.interpreter.Match;
import com.nyarian.self_education.patterns.behavioral.interpreter.Matches;
import com.nyarian.self_education.patterns.behavioral.interpreter.RegularExpression;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RegularExpressionFactoryTest {

    @Test
    @DisplayName("assert match if literal regex is equal to the source string")
    public void test1() {
        final String givenRegExp = "dog";
        final ExtendedExpression expression = ExtendedExpression.of(RegularExpressionFactory.create().of(givenRegExp));
        Assertions.assertEquals(Matches.full(givenRegExp), expression.matches(givenRegExp));
    }

    @Test
    @DisplayName("assert match absence if literal regex is bigger than the source string")
    public void test2() {
        final String givenRegExp = "dogs";
        final String givenSource = "dog";
        final ExtendedExpression expression = ExtendedExpression.of(RegularExpressionFactory.create().of(givenRegExp));
        assertFalse(expression.hasMatch(givenSource));
    }

    @Test
    @DisplayName("assert that single or expression matches the left side")
    public void test3() {
        final String givenRegExp = "dog|cat";
        final String givenSource = "dog";
        final ExtendedExpression expression = ExtendedExpression.of(RegularExpressionFactory.create().of(givenRegExp));
        assertEquals(Matches.full(givenSource), expression.matches(givenSource));
    }

    @Test
    @DisplayName("assert that single or expression matches the right side")
    public void test4() {
        final String givenRegExp = "dog|cat";
        final String givenSource = "cat";
        final ExtendedExpression expression = ExtendedExpression.of(RegularExpressionFactory.create().of(givenRegExp));
        assertEquals(Matches.full(givenSource), expression.matches(givenSource));
    }

    @Test
    @DisplayName("assert match absence if given literal does not hit any of \"or\" literal branches")
    public void test5() {
        final String givenRegExp = "dog|cat";
        final String givenSource = "god|tac";
        final ExtendedExpression expression = ExtendedExpression.of(RegularExpressionFactory.create().of(givenRegExp));
        assertFalse(expression.hasMatch(givenSource));
    }

    @Test
    @DisplayName("assert that repetition of a single character works as expected")
    public void test6() {
        final String givenRegExp = "a*";
        final String givenSource = "aaaa";
        final ExtendedExpression expression = ExtendedExpression.of(RegularExpressionFactory.create().of(givenRegExp));
        assertEquals(Matches.full(givenSource), expression.matches(givenSource));
    }

    @Test
    @DisplayName("assert that repetition of a two characters works as expected")
    public void test12() {
        final String givenRegExp = "ab*";
        final String givenSource = "abababab";
        final ExtendedExpression expression = ExtendedExpression.of(RegularExpressionFactory.create().of(givenRegExp));
        assertEquals(Matches.full(givenSource), expression.matches(givenSource));
    }

    @Test
    @DisplayName("assert that repetition of three characters and partial match works as expected")
    public void test13() {
        final String givenRegExp = "ab*";
        final String givenSource = "ababababba";
        final ExtendedExpression expression = ExtendedExpression.of(RegularExpressionFactory.create().of(givenRegExp));
        assertEquals(Match.of(givenSource, 0, 8), expression.matches(givenSource));
    }

    @Test
    @DisplayName("assert literal and repetition conjunction correctness")
    public void test7() {
        final String givenRegExp = "aa(b)*";
        final String givenSource = "aabbbbb";
        final ExtendedExpression expression = ExtendedExpression.of(RegularExpressionFactory.create().of(givenRegExp));
        assertEquals(Matches.full(givenSource), expression.matches(givenSource));
    }

    @Test
    @DisplayName("assert that chained \"or\" conditions are supported")
    public void test8() {
        final ExtendedExpression expression = ExtendedExpression.of(RegularExpression.of("dog|cat|human"));
        assertTrue(expression.hasMatch("dog"));
        assertTrue(expression.hasMatch("cat"));
        assertTrue(expression.hasMatch("human"));
    }

    @Test
    @DisplayName("assert that all variants of a triple \"or\" condition are working as expected")
    public void test9() {
        final String[] firstPair = {"dog", "cat"};
        final String[] secondPair = {"man", "woman"};
        final String[] thirdPair = {"left", "right"};
        final String regexp = String.format("(%s|%s)(%s|%s)(%s|%s)",
                firstPair[0], firstPair[1], secondPair[0], secondPair[1], thirdPair[0], thirdPair[1]);
        final ExtendedExpression expression = ExtendedExpression.of(RegularExpression.of(regexp));
        for (String first : firstPair) {
            for (String second : secondPair) {
                for (String third : thirdPair) {
                    assertTrue(expression.hasMatch(first + second + third));
                    assertFalse(expression.hasMatch(third + second + first));
                }
            }
        }
    }

    @Test
    @DisplayName("assert the correctness of \"or\" and repetition conjunction")
    public void test10() {
        final String regexp = "a*(b|c*)";
        final ExtendedExpression expression = ExtendedExpression.of(RegularExpression.of(regexp));
        assertTrue(expression.hasMatch("aaaab"));
        assertTrue(expression.hasMatch("ab"));
        assertTrue(expression.hasMatch("aaaacccc"));
        assertTrue(expression.hasMatch("acccc"));
        assertTrue(expression.hasMatch("aaaac"));
        assertTrue(expression.hasMatch("ac"));
    }

    @Test
    @DisplayName("assert than level 2 nested expressions are supportes and parsed correctly")
    public void test11() {
        final String regexp = "a*(b|(c)*)";
        final ExtendedExpression expression = ExtendedExpression.of(RegularExpression.of(regexp));
        assertTrue(expression.hasMatch("aaaab"));
        assertTrue(expression.hasMatch("ab"));
        assertTrue(expression.hasMatch("aaaacccc"));
        assertTrue(expression.hasMatch("acccc"));
        assertTrue(expression.hasMatch("aaaac"));
        assertTrue(expression.hasMatch("ac"));
    }

}
