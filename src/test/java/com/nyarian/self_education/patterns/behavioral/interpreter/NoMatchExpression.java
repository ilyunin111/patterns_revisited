package com.nyarian.self_education.patterns.behavioral.interpreter;

public class NoMatchExpression implements RegularExpression {

    public static final NoMatchExpression NOT_MATCHES = new NoMatchExpression();

    @Override
    public Match matches(MatchSequence subject) {
        throw new NoMatchException();
    }
}
